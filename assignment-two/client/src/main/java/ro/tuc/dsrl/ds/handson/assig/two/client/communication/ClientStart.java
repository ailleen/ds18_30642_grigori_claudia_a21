package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;
import ro.tuc.dsrl.ds.handson.assig.two.rpc.Naming;

import java.io.IOException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-client
 * @Since: Sep 24, 2015
 * @Description: Starting point of the Client application.
 */
public class ClientStart {
	private static final Log LOGGER = LogFactory.getLog(ClientStart.class);

	private ClientStart() {
	}

	public static void main(String[] args) throws IOException {
		ITaxService taxService = null;
		try {
			taxService = Naming.lookup(ITaxService.class, ServerConnection.getInstance());

			// 4; 180
            /*System.out.println(taxService.computeTax(new Car(2009, 100)));
			System.out.println("Tax value: " + taxService.computeTax(new Car(2009, 2000)));
			ServerConnection.getInstance().closeAll();*/

//            System.out.println("Tax value: " + taxService.computeTax(new Car(2009, 2000)));
//            System.out.println(taxService.computeTax(new Car(2009, -1)));
			//ServerConnection.getInstance().closeAll();

            Car car1 = new Car(2018, 2000, 12000);
            System.out.println(car1.toString() + " // Tax: " + taxService.computeTax(car1) + " // Selling price : " + taxService.computeSellingPrice(car1));

            Car car2 = new Car(2016, 2000, 7000);
            System.out.println(car2.toString() + " // Tax: " + taxService.computeTax(car2) + "// Selling price : " + taxService.computeSellingPrice(car2));

            Car car3 = new Car(2013, 1000, 600);
            System.out.println(car3.toString() + " // Tax: " + taxService.computeTax(car3) + "// Selling price : " + taxService.computeSellingPrice(car3));

            Car car4 = new Car(2015, 1500, 5000);
            System.out.println(car4.toString() + " // Tax: " + taxService.computeTax(car4) + "// Selling price : " + taxService.computeSellingPrice(car4));

            Car car5 = new Car(2015, 2900, 20000);
            System.out.println(car5.toString() + " // Tax: " + taxService.computeTax(car5) + "// Selling price : " + taxService.computeSellingPrice(car5));

            Car car6 = new Car(2009, -1, 10000);
            System.out.println(car6.toString() + " // Tax: " + taxService.computeTax(car6) + "// Selling price : " + taxService.computeSellingPrice(car6));

		} catch (Exception e) {
			LOGGER.error("",e);
			ServerConnection.getInstance().closeAll();
		}
	}
}
