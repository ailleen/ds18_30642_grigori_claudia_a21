package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-server
 * @Since: Sep 1, 2015
 * @Description:
 * 	Class used for computation of taxes to be paid for a specific car. An instance
 * 	of this class is published in the Registry so that it can be remotely accessed
 * 	by a client.
 */
public class TaxService implements ITaxService {

	public double computeTax(Car c) {
		// Dummy formula
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if(c.getEngineCapacity() > 1601) sum = 18;
		if(c.getEngineCapacity() > 2001) sum = 72;
		if(c.getEngineCapacity() > 2601) sum = 144;
		if(c.getEngineCapacity() > 3001) sum = 290;
		return c.getEngineCapacity() / 200.0 * sum;
	}

    @Override
    public double computeSellingPrice(Car c) {
        int yearDifference = 2018 - c.getYear();
        if (yearDifference >=  7){
            return 0;
        }

        double sellingPrice = 0d;
        Calendar calendar = Calendar.getInstance();
        if (c.getPurchasePrice() < 0) {
            throw new IllegalArgumentException("Purchase price must be positive.");
        } else if (c.getYear() > calendar.get(Calendar.YEAR)) {
            throw new IllegalArgumentException("Year cannot be greater than current year value.");
        } else {
            sellingPrice = c.getPurchasePrice() - ((c.getPurchasePrice() / 7) * yearDifference);
        }

        NumberFormat formatter = new DecimalFormat("#,###");
        String formattedString = formatter.format(sellingPrice);
        System.out.println(" >> " + formattedString);

        return sellingPrice;
    }
}
